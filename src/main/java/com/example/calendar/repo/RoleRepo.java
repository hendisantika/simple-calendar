package com.example.calendar.repo;

import com.example.calendar.models.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepo extends CrudRepository<Role, Integer> {

}
