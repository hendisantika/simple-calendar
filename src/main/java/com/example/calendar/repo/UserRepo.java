package com.example.calendar.repo;

import com.example.calendar.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<User, Integer> {
	User findByUsername(String username);
}
