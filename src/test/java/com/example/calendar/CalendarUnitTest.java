package com.example.calendar;

import com.example.calendar.models.Calendar;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalendarUnitTest {


	@Test
	public void testCalendarConstructor() {
		Calendar cal = new Calendar("test cal");

		assertEquals("test cal", cal.getName());

	}
}
