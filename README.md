## Simple Calendar

#### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/simple-calendar.git`
2. Navigate to the folder: `cd simple-calendar`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080/login

### Images Screen shot

Register Page

![Register Page](img/register.png "Register Page")

Login Page

![Login Page](img/login.png "Login Page")

Add Calendar Page

![Add Calendar Page](img/add-calendar.png "Add Calendar Page")

Add Event Page

![Add Event Page](img/add-event.png "Add Event Page")

View Event Page

![View Event Page](img/view.png "View Event Page")

View Calendar Page

![View Calendar Page](img/view-calendar.png "View Calendar Page")

Statistics Page

![Statistics Page](img/stat.png "Statistics Page")